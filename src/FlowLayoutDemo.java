/*
 * Write a program that meets the following requirements.

Create a frame and set its layout to FlowLayout
Create two panels and add them to the frame
Each panel contains three buttons. The panel uses FlowLayout.

Create a class named FlowLayoutDemo extends JFrame.
Create user interface using JPanels and JButtons. 
Add the instances of 3 JButtons using FlowLayout to JPanel named panel1 and other 3 instances of JButtons using FlowLayout to JPanel named panel2. 
Add panel1 and panel2 to JFrame using FlowLayout and set frame title to FlowLayout Demo.
Use pack method of JFrame, center the frame, and make the frame visible
The output should look like the screen below.
 */
import javax.swing.*;
import java.awt.*;
public class FlowLayoutDemo extends JFrame
{
	public FlowLayoutDemo()
	{				
		setLayout(new FlowLayout());		
		setSize(new Dimension(555,85));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		setLocationRelativeTo(null);
		
		JButton button1=new JButton("Button 1");
		JButton button2=new JButton("Button 2");
		JButton button3=new JButton("Button 3");
		JButton button4=new JButton("Button 4");
		JButton button5=new JButton("Button 5");
		JButton button6=new JButton("Button 6");
		JPanel panel1= new JPanel();
		JPanel panel2= new JPanel();
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel2.add(button4);
		panel2.add(button5);
		panel2.add(button6);
		add(panel1);
		add(panel2);
		
		setTitle("FlowLayout Demo");
		
	}
	public static void main(String[] args) 
	{
		FlowLayoutDemo flowLayoutTest = new FlowLayoutDemo();	
	}

}
